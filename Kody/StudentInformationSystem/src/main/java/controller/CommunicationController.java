/**
 * Provides communication between View and Server part. 
 * Creates a special query on basis of View actions, which is sent to server,
 * and if needed, receives the response of server, containing necessary info.
 */
package controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import model.Enrollment;
import model.SchoolUser;
import model.SchoolUserModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class CommunicationController {
    
    /** Boolean variable representing the state of controller request. */
    private boolean requestSent = false;
    
    /** String variable representing request that will be sent to server */
    //private String request;
    private String request;
    
    /** Server answer */
    private String result;
    
    private boolean resultError = false;

    public boolean isRequestSent() {
        return requestSent;
    }

    public void setRequestSent(boolean requestSent) {
        this.requestSent = requestSent;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }  
    
    public void sendRequest(JSONObject request) {
        
        setRequest(request.toString());
        setRequestSent(true);
        
    }

    public boolean isResultError() {
        return resultError;
    }

    public void setResultError(boolean resultError) {
        this.resultError = resultError;
    }
    
    public String analyzeReceivedMessage(String json) {
        JSONParser parser = new JSONParser();   
        JSONObject resultObj = null;
        JSONObject responseObj = new JSONObject();
        String response;

        //parse json from received result
        try {
            resultObj = (JSONObject) parser.parse(json);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Long id = Long.valueOf(1451);
        String operationCode = (String) resultObj.get("OperationCode");
        /*if (operationCode.equals("Generate")) {
            System.out.println("OLOLOLOLOLOLO MESSAGE RECEIVED");
            GenerateController genCon = new GenerateController();
            genCon.generateUsers();
            genCon.generateSubject();
            genCon.generateSchoolClass();
            responseObj.put("GenerateResult", "Success");
            response = responseObj.toString();
        } else {
            System.out.println("PYSCHPYSCHPYSCH NO MESAGE :(");
            response = "Error";
        }*/
        SubjectController subjectController = new SubjectController();
        SchoolUserController schoolUserController = new SchoolUserController();
        EnrollmentsStudentsController enrollmentsStudentsController = new EnrollmentsStudentsController();
        Long idOfStudent;
        Long idOfSubject;
        Long idOfEnrollment;
        int mark;
        
        switch(operationCode) {
            case "Generate":
                GenerateController genCon = new GenerateController();
                genCon.generateUsers();
                genCon.generateSubject();
                genCon.generateSchoolClass();
                responseObj.put("GenerateResult", "Success");
                responseObj.put("Error", "OK");                
                break;
                
            case "Login":
                LoginController logCon = new LoginController();
                String username = (String) resultObj.get("Username");
                String password = (String) resultObj.get("Password");
                JSONObject obj = logCon.login(username, password);
                Long receivedId = (Long) obj.get("ID");
                if (receivedId.equals(Long.valueOf(0))) {
                    responseObj.put("LoginResult", "Not success");
                    responseObj.put("Error","OK");                    
                } else {
                     //HttpSession session=request.getSession();  
                    currentId = receivedId;
                    System.out.println("Received id is" + currentId);
                    responseObj.put("ID", receivedId);
                    currentRole = (String) obj.get("Role");
                    responseObj.put("LoginResult", "Success");
                    responseObj.put("Error","OK");
                }                
                break;
                
            case "Current user info":
                System.out.println("Current id is " + id);
                UserProfileController upCon = new UserProfileController();
                responseObj.put("First name", upCon.printFirstName(id));
                responseObj.put("Last name", upCon.printLastName(id));
                responseObj.put("Address", upCon.printAddress(id));
                responseObj.put("Role", upCon.printRole(id));
                responseObj.put("User info result", "Success");
                responseObj.put("Error","OK");
                break;
            case "Schedule":
                ScheduleController schCon = new ScheduleController();
                responseObj = schCon.receiveSchedule(id);
                responseObj.put("Error", "OK");     
                break;
            case "Subjects":
                
                responseObj = subjectController.printListOfEnrollments(id);
                responseObj.put("Error", "OK");
                break;
            case "Students":
                
                responseObj = schoolUserController.printListOfStudents();
                responseObj.put("Error", "OK");
                break;
            case "Enrollments":
                idOfStudent = Long.parseLong((String) resultObj.get("ID").toString());                
                responseObj = schoolUserController.printListOfEnrollments(idOfStudent);
                responseObj.put("Error", "OK");
                break;
            case "All subjects":              
                responseObj = subjectController.printListOfAllSubjects();
                responseObj.put("Error", "OK");
                break;
            case "Add enrollment":        
                idOfStudent = Long.parseLong((String) resultObj.get("Student ID").toString()); 
                idOfSubject = Long.parseLong((String) resultObj.get("Subject ID").toString()); 
                enrollmentsStudentsController.addNewEnrollmentToStudent(idOfStudent, idOfSubject);
                responseObj.put("Error", "OK");
                break;
            case "Set grade for enrollment":        
                idOfEnrollment = Long.parseLong((String) resultObj.get("Enrollment ID").toString()); 
                idOfStudent = Long.parseLong((String) resultObj.get("Student ID").toString()); 
                mark = Integer.parseInt((String)resultObj.get("Mark").toString());
                enrollmentsStudentsController.addGradeToEnrollmentStudent(idOfStudent, idOfEnrollment, mark);
                responseObj.put("Error", "OK");
                break;
            case "Get grade for enrollment":        
                idOfEnrollment = Long.parseLong((String) resultObj.get("Enrollment ID").toString()); 
                idOfStudent = Long.parseLong((String) resultObj.get("Student ID").toString()); 
                mark = enrollmentsStudentsController.getGradeForEnrollmentStudent(idOfStudent, idOfEnrollment);
                responseObj.put("Grade", mark);
                responseObj.put("Error", "OK");
                break;
            case "Delete enrollment":        
                idOfStudent = Long.parseLong((String) resultObj.get("Student ID").toString()); 
                idOfSubject = Long.parseLong((String) resultObj.get("Enrollment ID").toString()); 
                enrollmentsStudentsController.deleteEnrollmentStudent(idOfStudent, idOfSubject);
                responseObj.put("Error", "OK");
                break;
            case "Best students":                
                responseObj = schoolUserController.printListOfBestStudents();
                responseObj.put("Error", "OK");
                break;
            default:
                responseObj.put("Error", "Error");
                break;
        }
        response = responseObj.toString();
        
        System.out.println(response);
        return response;
    }
    
    /***********************************************************************/
    /****************CURRENT USER CONTROLLER********************************/
    /***********************************************************************/
     /*
    * variables
    */
    private Long currentId = null;
    private String currentRole = null;
    private SchoolUser schoolUser = null;
    
    /*Getters/setters*/
    public SchoolUser getCurrentUser() {
        return schoolUser;
    }
    
    /*
    * informations for setting in this case are 
    * from database and other classes/instance made
    */
    public void setCurrentUser(Long id) {
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        schoolUser = schoolUserModel.selectSchoolUser(id);
        currentId = schoolUser.getId();
        currentRole = schoolUser.getUserRole();
    }

    public Long getCurrentId() {
        return currentId;
    }

    public void setCurrentId(Long currentId) {
        this.currentId = currentId;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }
    
    /*
    * clean is made by set nulls on values of methods
    */
    public void cleanCurrentUser() {
        setCurrentId(null);
        setCurrentRole(null);
        setCurrentUser(null);
    }
    
}
