/*
* CurrentUserController is needed for posibility
* to show to logged user specific informations.
*/
package controller;

import model.SchoolUser;
import model.SchoolUserModel;

public class CurrentUserController {

    /*
    * variables
    */
    private Long currentId;
    private String currentRole;
    private SchoolUser schoolUser;
    
    /*Getters/setters*/
    public SchoolUser getCurrentUser() {
        return schoolUser;
    }
    
    /*
    * informations for setting in this case are 
    * from database and other classes/instance made
    */
    public void setCurrentUser(Long id) {
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        schoolUser = schoolUserModel.selectSchoolUser(id);
        currentId = schoolUser.getId();
        currentRole = schoolUser.getUserRole();
    }

    public Long getCurrentId() {
        return currentId;
    }

    public void setCurrentId(Long currentId) {
        this.currentId = currentId;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }
    
    /*
    * clean is made by set nulls on values of methods
    */
    public void cleanCurrentUser() {
        setCurrentId(null);
        setCurrentRole(null);
        setCurrentUser(null);
    }
}
