
package controller;

import java.util.Collection;
import java.util.List;
import model.Enrollment;
import model.EnrollmentModel;
import model.EnrollmentStudent;
import model.EnrollmentStudentModel;
import model.GradeModel;
import model.SchoolUser;
import model.SchoolUserModel;
import model.Subject;
import model.SubjectModel;
import static model.Subject_.subjectName;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class EnrollmentsStudentsController {
    
    public void addNewEnrollmentToStudent(Long user_id, Long subject_id) {
        
        String userId = user_id.toString();
        String subjectId = subject_id.toString();
        System.out.println("Id user " + userId);
        System.out.println("Subject id "+subjectId);
        
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        Long enrollment_id = enrollmentModel.createEnrollment();
        
        enrollmentModel.updateEnrollmentSubject(enrollment_id,subject_id);
        enrollmentModel.updateEnrollmentStudent(enrollment_id, user_id);        
    }
    
    public void addExistionEnrollmentToStudent(Long user_id, Long enrollment_id) {
        
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        enrollmentModel.updateEnrollmentStudent(enrollment_id, user_id);
        
    }
    
    public JSONObject receiveListOfEnrollmentsForStudent(Long user_id) {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        Collection<Enrollment> enrollmentsList = schoolUserModel.selectStudentListOfEnrollments(user_id);
        
        for (Enrollment enrollment : enrollmentsList) {            
            Subject subject = enrollment.getSubject();
            String subjectName = subject.getSubjectName();
            
            array.add(subjectName);
        }
        obj.put("Enrollments list", array);
        
        return obj;        
    }
    
    
    public JSONObject receiveListOfStudentsForEnrollment(Long enrollment_id) {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        Collection<SchoolUser> studentsList = enrollmentModel.selectEnrollmentListOfStudents(enrollment_id);
        
        for (SchoolUser student : studentsList) {            
            String name = student.getFirstName() + " " + student.getLastName();
            
            array.add(name);
        }
        obj.put("Students list", array);
        
        return obj;        
    }
    
    public void addGradeToEnrollmentStudent(Long user_id, Long enrollment_id, int mark) {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        SchoolUser student = new SchoolUser();
        student = schoolUserModel.selectSchoolUser(user_id);
        List<EnrollmentStudent> enrollmentStudentList = student.getEnrollmentStudent();
        System.out.println("Received mark is" + mark);
        
        for (EnrollmentStudent enrollmentStudent : enrollmentStudentList) {            
            if (((enrollmentStudent.getEnrollment()).getId()).equals(enrollment_id)) {
                if (enrollmentStudent.getGrade() == null) {
                    GradeModel gradeModel = new GradeModel();
                    gradeModel.createGrade(mark, enrollmentStudent);
                } else {
                    (enrollmentStudent.getGrade()).setMark(mark);
                }        
                System.out.println("Written mark is " + enrollmentStudent.getGrade().getMark());
                
            }
        }       
    }
    public int getGradeForEnrollmentStudent(Long user_id, Long enrollment_id) {
        
        int mark = 0;
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        SchoolUser student = new SchoolUser();
        student = schoolUserModel.selectSchoolUser(user_id);
        List<EnrollmentStudent> enrollmentStudentList = student.getEnrollmentStudent();
        
        for (EnrollmentStudent enrollmentStudent : enrollmentStudentList) {            
            if (((enrollmentStudent.getEnrollment()).getId()).equals(enrollment_id)) {
                if (enrollmentStudent.getGrade()!= null) {
                    mark = (enrollmentStudent.getGrade()).getMark();
                }
            }
        } 
        return mark;
    }
    public void deleteEnrollmentStudent(Long user_id, Long enrollment_id) {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        SchoolUser student = new SchoolUser();
        student = schoolUserModel.selectSchoolUser(user_id);
        List<EnrollmentStudent> enrollmentStudentList = student.getEnrollmentStudent();
        
        for (EnrollmentStudent enrollmentStudent : enrollmentStudentList) {            
            if (((enrollmentStudent.getEnrollment()).getId()).equals(enrollment_id)) {
                EnrollmentStudentModel esm = new EnrollmentStudentModel();
                esm.deleteEnrollmentStudent(enrollmentStudent.getId());
            }
        } 
    }
    
}
