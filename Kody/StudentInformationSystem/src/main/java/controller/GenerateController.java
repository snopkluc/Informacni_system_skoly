/**
 * Testing method for generating data 
 */
package controller;

import model.SchoolClass;
import model.SchoolClassModel;
import model.SchoolUserModel;
import model.SubjectModel;
import utils.PasswordHash;



public class GenerateController {
    
    public void generateUsers() {
        
        SchoolUserModel suModel = new SchoolUserModel();
        
        try {
            suModel.createSchoolUser("petrnovak", PasswordHash.getHex("petrnovak"), "Petr", "Novák", "Vinohradská 322 Praha 2 14533", "student" );
            suModel.createSchoolUser("davidmarek", PasswordHash.getHex("davidmarek"), "David", "Marek", "Vinohradská 81 Praha 2 14533", "student" );
            suModel.createSchoolUser("martinaodvarkova", PasswordHash.getHex("martinaodvarkova"), "Martina", "Odvárková", "Krajská 2 Praha 2 14533", "student" );
            suModel.createSchoolUser("petravacikova", PasswordHash.getHex("petravacikova"), "Petra", "Vacíková", "Volanská 36 Praha 3 14300", "student" );
            suModel.createSchoolUser("michalpasek", PasswordHash.getHex("michalpasek"), "Michal", "Pašek", "Pučovská 7 Praha 2 14533", "student" );
            suModel.createSchoolUser("marketamisikova", PasswordHash.getHex("marketamisikova"), "Markéta", "Mišíková", "Kajenská 339 Praha 5 14015", "student" );
            suModel.createSchoolUser("jansedlak", PasswordHash.getHex("jansedlak"), "Jan", "Sedlák", "Vinohradská 5 Praha 2 14300", "teacher" );
            suModel.createSchoolUser("ludmilavlastova", PasswordHash.getHex("ludmilavlastova"), "Ludmila", "Vlastová", "Přeštická 8 Praha 5 13401", "teacher" );
            suModel.createSchoolUser("kristynaschejbalova", PasswordHash.getHex("kristynaschejbalova"), "Kristýna", "Schejbalová", "Kolárova 904 Praha 1 14415", "teacher" );
            suModel.createSchoolUser("martinajandova", PasswordHash.getHex("martinajandova"), "Martina", "Jandová", "Vodnická 21 Praha 4 14000", "teacher" );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
                
    }
    
    public void generateSubject() {
        
        SubjectModel subjectModel = new SubjectModel();
        
        subjectModel.createSubject("Matematika");
        subjectModel.createSubject("Český jazyk");
        subjectModel.createSubject("Anglický Jazyk");
        subjectModel.createSubject("Tělesná výchova");
        subjectModel.createSubject("Ruční práce");
        subjectModel.createSubject("Výtvarná výchova");
        
    }
    
    public void generateSchoolClass() {
        
        SchoolClassModel schoolClassModel = new SchoolClassModel();
        
        schoolClassModel.createSchoolClass("1A", "2016/2017");
        schoolClassModel.createSchoolClass("2A", "2016/2017");
        schoolClassModel.createSchoolClass("3A", "2016/2017");
        
    }
    
    /*public void deleteSchoolUser() {
        SchoolUserModel suModel = new SchoolUserModel();
        
        suModel.deleteSchoolUser(Long.valueOf(501));
        suModel.deleteSchoolUser(Long.valueOf(551));
        suModel.deleteSchoolUser(Long.valueOf(601));
        suModel.deleteSchoolUser(Long.valueOf(651));
        
    }*/
    
    public void setAllUsersClass() {
        SchoolClassModel schoolClassModel = new SchoolClassModel();
        SchoolUserModel suModel = new SchoolUserModel();
        SchoolClass schoolClass = schoolClassModel.selectSchoolClass(Long.valueOf(2051));
        
        for (int i = 1151; i < 1452; i+=50) {
            suModel.updateStudentSchoolClass(Long.valueOf(i), schoolClass.getId());            
        }
    }
    
}
