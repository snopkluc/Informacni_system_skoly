
package controller;

import model.Enrollment;
import model.EnrollmentModel;
import model.EnrollmentStudent;
import model.EnrollmentStudentModel;
import model.Grade;
import model.GradeModel;

public class GradeController {
    
    public void addGrade(int grade, int enrollmentStudent_id) {
        GradeModel gradeModel = new GradeModel();
        EnrollmentStudentModel enrollmentStudentModel = new EnrollmentStudentModel();
        EnrollmentStudent enrollmentStudent = enrollmentStudentModel.selectEnrollmentStudent(Long.valueOf(enrollmentStudent_id));
        
        gradeModel.createGrade(grade, enrollmentStudent);
    }
    
    public void changeGrade(int grade_id, int newMark) {
        GradeModel gradeModel = new GradeModel();
        (gradeModel.selectGrade(Long.valueOf(grade_id))).setMark(newMark);
    }
    
    public int displayGrade(int grade_id) {
        GradeModel gradeModel = new GradeModel();
        Grade grade = gradeModel.selectGrade(Long.valueOf(grade_id));
        return grade.getMark();
    }
    
}
