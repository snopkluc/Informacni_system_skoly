/*
 * Class with methods for login 
 */
package controller;

import model.LoginModel;
import org.json.simple.JSONObject;

public class LoginController {
    private String username;
    private String controller;
    private boolean error;
    private String errorMessage;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public boolean isLogined() {
        return false;
    }
    
    public JSONObject login(String username, String password) {
        
        boolean passwordMatches = false;        
        LoginModel loginModel = new LoginModel();
        Long currentId = Long.valueOf(0);
        JSONObject obj = new JSONObject();
        String message = "";
        
        boolean usernameExists = loginModel.checkUsername(username);
        if (usernameExists) {
            passwordMatches = loginModel.checkPassword(username,password);            
            if (passwordMatches) {
                obj = loginModel.loginGetID(username, password);
            } else {
                obj.put("ID",currentId);
            }            
        } else {
            obj.put("ID",currentId);
        } 
        return obj;
    }
    
    public void Logout() {
        
    }
}
