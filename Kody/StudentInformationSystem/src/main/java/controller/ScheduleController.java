
package controller;

import java.util.Collection;
import java.util.List;
import model.Enrollment;
import model.Lesson;
import model.SchoolUser;
import model.SchoolUserModel;
import model.Subject;
import org.json.simple.JSONObject;


public class ScheduleController {
    
    /** Returns schedule for specified user in JSON format, where data are set
     * as pair lesson timecode - name of a subject.
     * @param user_id Long
     * @return JSONObject
     */
    public JSONObject receiveSchedule(Long user_id) {
        JSONObject obj = new JSONObject();
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        List<Enrollment> enrollmentsList = schoolUserModel.selectStudentListOfEnrollments(user_id);
        for (Enrollment enrollment : enrollmentsList) {
            Subject subject = enrollment.getSubject();
            String subjectName = subject.getSubjectName();
            
            List<Lesson> lessonsList = enrollment.getLessons();
            for (Lesson lesson : lessonsList) {
                String lessonTimecode = lesson.getTimeCode();
                obj.put(lessonTimecode, subjectName);
            }            
        }        
        return obj;
    }
    
}
