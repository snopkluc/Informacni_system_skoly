/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Enrollment;
import model.SchoolUser;
import model.SchoolUserModel;
import model.Subject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SchoolUserController {
    
    public JSONObject printListOfStudents() {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        List<SchoolUser> listOfStudents = schoolUserModel.selectAllStudents();
        for (SchoolUser student : listOfStudents) {
            
            JSONObject studentInfo = new JSONObject();
            studentInfo.put("Name",student.getFirstName() + " " + student.getLastName());
            studentInfo.put("ID", student.getId());
            array.add(studentInfo);         
            
        }        
        obj.put("Students", array);       
        
        return obj;
        
    }
    
    public JSONObject printListOfBestStudents() {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        List<SchoolUser> listOfStudents = schoolUserModel.selectAllStudents();
        for (SchoolUser student : listOfStudents) {
            
            JSONObject studentInfo = new JSONObject();
            studentInfo.put("Name",student.getFirstName() + " " + student.getLastName());
            studentInfo.put("ID", student.getId());
            array.add(studentInfo);         
            
        }        
        obj.put("Students", array);       
        
        return obj;
        
    }
    
    public JSONObject printListOfEnrollments(Long id) {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        List<Enrollment> listOfEnrollments = schoolUserModel.selectStudentListOfEnrollments(id);
        for (Enrollment enrollment : listOfEnrollments) {
            
            Subject subject = new Subject();
            subject = enrollment.getSubject();
            
            JSONObject enrollmentInfo = new JSONObject();
            enrollmentInfo.put("Name",subject.getSubjectName());
            enrollmentInfo.put("ID", enrollment.getId());
            array.add(enrollmentInfo);         
            
        }        
        obj.put("Enrollments", array);       
        
        return obj;
        
    }
    
}
