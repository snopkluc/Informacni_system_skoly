
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Enrollment;
import model.Lesson;
import model.SchoolUserModel;
import model.Subject;
import model.SubjectModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SubjectController {
    
    public void addSubjectToEnrollments() {
        
    }

    public JSONObject printListOfEnrollments(Long id) {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        List<Enrollment> listOfEnrollments = schoolUserModel.selectStudentListOfEnrollments(id);
        for (Enrollment enrollment : listOfEnrollments) {
            Subject subject = enrollment.getSubject();
            String subjectName = subject.getSubjectName();
            array.add(subjectName);           
        }        
        obj.put("Subjects", array);       
        
        return obj;
        
    }
    
    public JSONObject printListOfAllSubjects() {
        
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        
        SubjectModel subjectModel = new SubjectModel();
        List<Subject> listOfSubjects = subjectModel.selectAllSubjects();
        System.out.println("Subjects received");
        for (Subject subject : listOfSubjects) {
            
            JSONObject subjectInfo = new JSONObject();            
            String subjectName = subject.getSubjectName();
            System.out.println("Subject name is " + subjectName);
            subjectInfo.put("Name", subjectName);
            subjectInfo.put("ID", subject.getId());
            
            array.add(subjectInfo);   
            
        }        
        obj.put("Subjects", array);       
        
        return obj;
        
    }
    
}
