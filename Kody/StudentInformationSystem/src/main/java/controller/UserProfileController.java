/**
 * Class contains methods allowing to extract necessary data for the current user.
 */
package controller;

import model.SchoolUserModel;

public class UserProfileController {
    
    SchoolUserModel userModel = new SchoolUserModel();
    
    public String printFirstName(Long id) {
        String firstName = (userModel.selectSchoolUser(id)).getFirstName();
        return firstName;
        
    }
    
    public String printLastName(Long id) {
        String lastName = (userModel.selectSchoolUser(id)).getLastName();
        return lastName;
    }
    
    public String printUsername(Long id) {
        String username = (userModel.selectSchoolUser(id)).getUsername();
        return username;
    }
    
    public String printAddress(Long id) {
        String address = (userModel.selectSchoolUser(id)).getAddress();
        return address;
    }
    
    public String printRole(Long id) {
        String role = (userModel.selectSchoolUser(id)).getUserRole();
        return role;
    }
    
    
    
}
