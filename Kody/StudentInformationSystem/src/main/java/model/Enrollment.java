/**
 * Represents entity Enrollment in database.
 * This class was made to complete all informations about one specific lesson,
 * teacher who lecture it, in a class etc. It's meant for work with subject which is 
 * added to a concrete class with concrete teacher and day and hour.
 * It's different from subject, because subject has just name, it may has a teacher
 * and difficulty but enrollment is concrete lecture of concrete class with 
 * concrete teacher.
 */
package model;


import java.util.Collection;
import java.util.List;
import javax.persistence.*;
/*select u.last_name, u.first_name, u.id, c.year, c.number, avg(g.mark) as avg_mark

from school_user u

join enrollment_students s
on s.student_id = u.id

join enrollment e
on s.enrollment_id = e.id

join grade g
on s.grade = g.id

left join (select s1.student_id
           
           from enrollment_students s1
           
           join enrollment e1
		   on s1.enrollment_id = e1.id
           and e.academic_year = '2015/2016'
           
           left join grade g1
           on s1.grade_id = g1.id
           
           where g1.mark is null
             or g1.mark > 2) x
on x.student_id = u.id

join school_class c
on u.schoolclass_id = c.id

where x.student_id is null
  and e.academic_year = '2015/2016'

group by u.last_name, u.first_name, u.id, c.year, c.number

having avg(g.mark) <= 1.5

order by avg_mark, c.year desc, u.last_name, u.first_name*/
/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="enrollment")
public class Enrollment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;    

    @Column(name="academic_year")
    private String academicYear;
    
    @ManyToOne
    private Subject subject;
    
    @ManyToOne
    private SchoolUser teacher;
    
    @OneToMany(mappedBy="enrollment")
    private List<Lesson> lessons;
    
    @OneToMany(mappedBy="enrollment")
    private List<EnrollmentStudent> enrollmentStudent;
    
    
    /*@ManyToMany
    @JoinTable(name="enrollment_student",
               joinColumns=@JoinColumn(name="enrollment_id"),
               inverseJoinColumns=@JoinColumn(name="student_id"))
    private Collection<SchoolUser> students;*/
    
    /**
     * Basic constructor
     */
    public Enrollment() { 
        
    }
    
    /**
     * Extended constructor
     * @param academicYear String
     * @param subject Subject
     * @param teacher SchoolUser
     */
    public Enrollment(String academicYear, Subject subject, SchoolUser teacher) {
        this.academicYear = academicYear;
        this.subject = subject;
        this.teacher = teacher;
    }
    
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    /*public Collection<SchoolUser> getStudents() {
        return students;
    }

    public void setStudents(Collection<SchoolUser> students) {
        this.students = students;
    }*/

    public List<EnrollmentStudent> getEnrollmentStudent() {
        return enrollmentStudent;
    }

    public void setEnrollmentStudent(List<EnrollmentStudent> enrollmentStudent) {
        this.enrollmentStudent = enrollmentStudent;
    }  

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public SchoolUser getTeacher() {
        return teacher;
    }

    public void setTeacher(SchoolUser teacher) {
        this.teacher = teacher;
    }
    
}
