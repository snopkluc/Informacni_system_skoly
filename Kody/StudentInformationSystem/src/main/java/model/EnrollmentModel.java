/**
 * Provides methods for DB connection and pulling required information from DB.
 * JPA was used
 */
package model;

import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class EnrollmentModel {
    
    /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in Enrollment table with single ID
     */
    public Long createEnrollment() {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        Enrollment enrollment = new Enrollment();
        em.persist(enrollment);
                
        Long id = enrollment.getId();
        et.commit();       
        
        em.close();
        emf.close();
        return id;
    }   
    
    /**
     * Pulls required Enrollment by it's ID
     * @param id Long of required enrollment
     * @return Enrollment, object created from DB record
     */
    public Enrollment selectEnrollment(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        Enrollment enrollment = em.find(Enrollment.class, id);   
        
        em.close();
        emf.close();
        return enrollment;
    }
    
    public List<SchoolUser> selectEnrollmentListOfStudents(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id);  
        //find all enrollmentstudents
        String query = "SELECT s FROM Enrollment e"
                + "     JOIN e.enrollmentStudent es"
                + "     JOIN es.student s"
                + "     WHERE e.id="+id;
        Query tq = em.createQuery(query);
        List<SchoolUser> listOfStudents = tq.getResultList();
        
        em.close();
        emf.close();
        return listOfStudents;
    }
    
    /**
     * Adds students to enrollment. 
     * As the Enrollment and SchoolUser(student) entities have M:N relationship, 
     * so an additional JOIN table is required to connect them. Method gets lists 
     * of students from enrollment and enrollments from student, and adds new student 
     * and new enrollment.
     * @param id_enrollment Long
     * @param id_student Long
     */
    public void updateEnrollmentStudent(Long id_enrollment, Long id_student) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);  
        SchoolUser student = em.find(SchoolUser.class, id_student);
        EnrollmentStudent enrollmentStudent = new EnrollmentStudent();
               
        et.begin();
        enrollmentStudent.setEnrollment(enrollment);
        enrollmentStudent.setStudent(student);
        student.getEnrollmentStudent().add(enrollmentStudent);
        em.persist(enrollmentStudent);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    
    /**
     * Methods updates academic year in Enrollment
     * @param id Long id of required enrollment
     * @param academicYear String
     */
    public void updateEnrollmentAcademicYear(Long id, String academicYear) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id);
               
        et.begin();
        enrollment.setAcademicYear(academicYear);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    /**
     * Method updates teacher who teaches current Enrollment
     * @param id_enrollment Long
     * @param id_teacher Long
     */
    public void updateEnrollmentTeacher(Long id_enrollment, Long id_teacher) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);  
        SchoolUser teacher = em.find(SchoolUser.class, id_teacher);
               
        et.begin();
        enrollment.setTeacher(teacher);
        teacher.getEnrollmentsTeaching().add(enrollment);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    /**
     * Method updates subject of Enrollments. As this method is already created 
     * in SubjectModel, so it creates an instance of SubjectModel and uses the
     * same method
     * @param id_enrollment Long
     * @param id_subject Long
     */
    public void updateEnrollmentSubject(Long id_enrollment, Long id_subject) {
        
        SubjectModel subjectModel = new SubjectModel();
        subjectModel.addSubjectEnrollment(id_subject, id_enrollment);
      
    }
    

    /**
     * Method adds a lesson to the list of Lessons of current Enrollment
     * @param id_enrollment Long
     * @param id_lesson Long
     */
    public void addEnrollmentLesson(Long id_enrollment, Long id_lesson) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Lesson lesson = em.find(Lesson.class, id_lesson);
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);
        
        et.begin();
        enrollment.getLessons().add(lesson);
        lesson.setEnrollment(enrollment);       
        et.commit();
        
        em.close();
        emf.close();
    }
    
    /**
     * Deletes current enrollment by it's ID from DB
     * @param id Long
     */
    public void deleteEnrollment(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id);  
        //find all enrollmentstudents
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete criteriaDelete = criteriaBuilder.createCriteriaDelete(EnrollmentStudent.class);
        Root enrollmentStudent = criteriaDelete.from(EnrollmentStudent.class);
        criteriaDelete.where(criteriaBuilder.equal(enrollmentStudent.get("enrollment_id"), id));
        Query query = em.createQuery(criteriaDelete);
        int rowCount = query.executeUpdate();
               
        et.begin();
        em.remove(enrollment);
        et.commit();
        
        em.close();
        emf.close();
    }
    
}
