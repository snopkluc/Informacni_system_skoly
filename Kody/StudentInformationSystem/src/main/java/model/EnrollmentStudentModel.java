
package model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class EnrollmentStudentModel {

    /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
        
    /**
     * Pulls required EnrollmentStudent by it's ID
     * @param id Long of required enrollment
     * @return Enrollment, object created from DB record
     */
    public EnrollmentStudent selectEnrollmentStudent(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);   
        
        em.close();
        emf.close();
        return enrollmentStudent;
    }    
    
    public Grade getEnrollmentStudentGrade(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);  
        Grade grade = enrollmentStudent.getGrade();
        
        em.close();
        emf.close();
        return grade;
    }  

    /*public void updateEnrollmentStudentGrade(Long id, Long id_grade) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);  
        Grade grade = em.find(Grade.class, id_grade);
               
        et.begin();
        enrollmentStudent.setGrade(grade);
        em.persist(enrollmentStudent);
        et.commit();
        
        em.close();
        emf.close();
    }  */
    public void updateEnrollmentStudentGrade(Long id, int mark) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);  
        Grade grade = enrollmentStudent.getGrade();
               
        et.begin();
        grade.setMark(mark);
        em.persist(grade);
        et.commit();
        
        em.close();
        emf.close();
    }  
    
    public void deleteEnrollmentStudentGrade(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);  
        Grade grade = enrollmentStudent.getGrade();
               
        et.begin();
        em.remove(grade);
        et.commit();
        
        em.close();
        emf.close();
    }  
   
    public void deleteEnrollmentStudent(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        EnrollmentStudent enrollmentStudent = em.find(EnrollmentStudent.class, id);   
               
        et.begin();
        em.remove(enrollmentStudent);
        et.commit();
        
        em.close();
        emf.close();
        
    }
}
