/*
 This class gets informations about students and techers and subjets
 because difficulty of subject is the same as grade so it's used in other
 parts of program
*/
package model;


import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="grade")
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="mark", nullable=false)//wtf name of column? changed to mark, control on DB
    private int mark;
    
    @OneToOne(mappedBy="grade")
    private EnrollmentStudent enrollmentStudent;
    
     /**
     * Basic constructor
     */
    public Grade(){
        
    }
      /**
     * Extended constructor
     * @param grade int
     * @param enrollment Enrollmen
     */
    public Grade(int grade, EnrollmentStudent enrollmentStudent) {
        this.mark = grade;
        this.enrollmentStudent = enrollmentStudent;
    }

    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public EnrollmentStudent getEnrollmentStudent() {
        return enrollmentStudent;
    }

    public void setEnrollmentStudent(EnrollmentStudent enrollmentStudent) {
        this.enrollmentStudent = enrollmentStudent;
    }
    
    
    

    
}
