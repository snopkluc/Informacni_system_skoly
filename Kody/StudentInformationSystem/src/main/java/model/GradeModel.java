/* 
 Same posibilites as Grade Class only allows us approach to
 informations saved in database
 */
package model;

import javax.persistence.*;

public class GradeModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in Grade table with single ID
     */
    public void createGrade(int grade, EnrollmentStudent enrollmentStudent) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        Grade gradeObject = new Grade();
        em.persist(gradeObject);
        gradeObject.setMark(grade);
        gradeObject.setEnrollmentStudent(enrollmentStudent);
        enrollmentStudent.setGrade(gradeObject);
        
        
        et.commit();     
        
        em.close();
        emf.close();
    }
    
    /**
     * Pulls required Grade by it's ID
     * @param id Long of required grade
     * @return Grade, object created from DB record
     */
    public Grade selectGrade(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        Grade grade = em.find(Grade.class, id);   
        
        em.close();
        emf.close();
        return grade;
    }
    
     /**
     * Methods updates grade of Enrollment
     * @param id_grade Long id of required enrollment
     * @param id_enrollment Long
     */
    /*public void updateGradeEnrollment(Long id_grade, Long id_enrollment) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);  
        Grade grade = em.find(Grade.class, id_grade);
               
        et.begin();
        grade.setEnrollment(enrollment);
        enrollment.setGrade(grade);
        et.commit();
        
        em.close();
        emf.close();
    }*/
    
    /**
     * Methods updates mark 
     * @param id_grade Long id of required enrollment
     * @param mark int
     */
    public void updateGradeMark(Long id_grade, int mark) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Grade grade = em.find(Grade.class, id_grade);
               
        et.begin();
        grade.setMark(mark);
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Deletes current grade by it's ID from DB
     * @param id Long
     */
    public void deleteGrade(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Grade grade = em.find(Grade.class, id);   
               
        et.begin();
        em.remove(grade);
        et.commit();
        
        em.close();
        emf.close();
    }
}
    
