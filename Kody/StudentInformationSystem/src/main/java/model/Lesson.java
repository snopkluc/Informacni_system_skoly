/* 
  Lesson is a class usses entities as subject with their own parameters
  as time and day, how long lesson lasts so it's possible to make a schedule of any class.
  It dipendents on school year because the year describes difficulty of the specific lecture.
  which is added directly to subject.
*/
package model;

import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="lesson")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    //time code in a week - A1-E5, defines exact time of lesson
    @Column(name="time_code", nullable=false)
    private String timeCode;

    @ManyToOne
    private Enrollment enrollment;
    
     /**
     * Basic constructor
     */
    public Lesson() {
        
    }
     
    /**
     * Extended constructor
     * @param timeCode String
     */
    public Lesson(String timeCode) {
        this.timeCode = timeCode;
    }
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTimeCode() {
        return timeCode;
    }

    public void setTimeCode(String timeCode) {
        this.timeCode = timeCode;
    }

    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }
    
}
