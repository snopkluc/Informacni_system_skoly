/*
  Same posibilites as Lesson Class only allows us approach to
 informations saved in database
 */
package model;

import javax.persistence.*;

public class LessonModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in Lesson table with single ID
     */
    public void createLesson(String timeCode, Enrollment enrollment) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        Lesson lesson = new Lesson();
        lesson.setTimeCode(timeCode);
        lesson.setEnrollment(enrollment);
        
        
        em.persist(lesson);
        et.commit();     
        
        em.close();
        emf.close();
    }
    
    /**
     * Pulls required Lesson by it's ID
     * @param id Long of required lesson
     * @return Lesson, object created from DB record
     */
    public Lesson selectLesson(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        Lesson lesson = em.find(Lesson.class, id);   
        
        em.close();
        emf.close();
        return lesson;
    }
    
    /**
     * Methods updates Timecode of Lesson
     * @param id_lesson Long id of required enrollment
     * @param timecode String
     */ 
    public void updateLessonTimecode(Long id_lesson, String timecode) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Lesson lesson = em.find(Lesson.class, id_lesson);
               
        et.begin();
        lesson.setTimeCode(timecode);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    /**
     * Method adds a lesson to the list of Lessons of current Enrollment
     * @param id_lesson Long
     * @param id_enrollment Long
     */
    //check it
    public void addLessonEnrollment(Long id_lesson, Long id_enrollment) {
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        enrollmentModel.addEnrollmentLesson(id_enrollment, id_lesson);
        
        /*emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Lesson lesson = em.find(Lesson.class, id_lesson);
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);
        
        et.begin();
        enrollment.getLessons().add(lesson);
        lesson.setEnrollment(enrollment);       
        et.commit();
        
        em.close();
        emf.close();*/
    }
    
    
    /**
     * Deletes current lesson by it's ID from DB
     * @param id Long
     */
    public void deleteLesson(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Lesson lesson = em.find(Lesson.class, id);   
               
        et.begin();
        em.remove(lesson);
        et.commit();
        
        em.close();
        emf.close();
    }
}
