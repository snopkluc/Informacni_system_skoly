/*
 Gets users informations and checks match with informations in database.
*/
package model;

import javax.persistence.*;
import org.json.simple.JSONObject;

public class LoginModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
   /** Method that checks if received username exists in db */
    public boolean checkUsername(String username) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        //Query tq = em.createQuery("SELECT COUNT(su.id) FROM SchoolUser su WHERE username = ':param'");
        Query tq = em.createQuery("SELECT COUNT(su.id) FROM SchoolUser su WHERE su.username = '"+username+"'");
        //tq.setParameter("param", username);
        Long numberOfUsernames = (Long)tq.getSingleResult();
        em.close();
        emf.close();
        if (numberOfUsernames == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /** Method compares received password with password from db */
    public boolean checkPassword(String username, String password) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        //Query tq = em.createQuery("SELECT password FROM SchoolUser WHERE username = ':param'");
        Query tq = em.createQuery("SELECT su.password FROM SchoolUser su WHERE su.username = '"+username+"'");
       // tq.setParameter("param", username);
        String selectedPassword = (String)tq.getSingleResult();
        em.close();
        emf.close();
        
        if(selectedPassword.equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    /** Method returns JSON with current user ID and role */
    public JSONObject loginGetID(String username, String password) {
        
        Long id = Long.valueOf(0);
        JSONObject obj = new JSONObject();
        
        if(checkUsername(username) && (checkPassword(username, password))) {
            
            emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
            em = emf.createEntityManager();   

            //Query tq = em.createQuery("SELECT id FROM SchoolUser WHERE username = ':param'");
            Query tq = em.createQuery("SELECT su.id FROM SchoolUser su WHERE su.username = '"+username+"'");
            //tq.setParameter("param", username);
            Long selectedId = (Long)tq.getSingleResult();
            obj.put("ID", selectedId);
            
            tq = em.createQuery("SELECT su.userRole FROM SchoolUser su WHERE su.username = '"+username+"'");
            String role = (String)tq.getSingleResult();
            obj.put("Role", role);
            
            em.close();
            emf.close();
            
        } else {
            obj.put("ID", id);
        }      
        
        return obj;
        
    }
}
