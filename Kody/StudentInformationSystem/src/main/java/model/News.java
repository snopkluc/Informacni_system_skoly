/* 
 The role Teacher will let us edit and add news, actions etc. 
 it's gonna be shown on main page.
*/
package model;

import java.sql.Timestamp;
import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="news_header", nullable=false)
    private String newsHeader;
    
    @Column(name="news_text", nullable=false)
    private String newsText;
    
    @Column(name="created_on", nullable=false)
    private Timestamp createdOn;
    
    @ManyToOne
    private SchoolUser teacher;

     /**
     * Basic constructor
     */
    public News() {
        
    }
    
      /**
     * Extended constructor
     * @param newsHeader String
     * @param newsText String
     * @param createdOn Timestamp
     */
    public News(String newsHeader, String newsText, Timestamp createdOn) {
        this.newsHeader = newsHeader;
        this.newsText = newsText;
        this.createdOn = createdOn;
    }
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewsHeader() {
        return newsHeader;
    }

    public void setNewsHeader(String newsHeader) {
        this.newsHeader = newsHeader;
    }

    public String getNewsText() {
        return newsText;
    }

    public void setNewsText(String newsText) {
        this.newsText = newsText;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public SchoolUser getTeacher() {
        return teacher;
    }

    public void setTeacher(SchoolUser teacher) {
        this.teacher = teacher;
    }    
}
