/*
  Same posibilites as News Class only allows us approach to
  informations saved in database
 */
package model;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;

public class NewsModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in News table with single ID
     */
    public void createNews(String newsHeader, String newsText, SchoolUser teacher, Timestamp createdOn) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        News news = new News();
        news.setNewsHeader(newsHeader);
        news.setNewsText(newsText);
        news.setCreatedOn(createdOn);
        news.setTeacher(teacher);
        
        em.persist(news);
        et.commit();     
        
        em.close();
        emf.close();
    }
    
     /**
     * Pulls required News by it's ID
     * @param id Long of required news
     * @return listOfNews, object created from DB record
     */
    public List<News> selectAllNews() {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        TypedQuery tq = em.createQuery(
                "SELECT n FROM News AS n", 
                News.class
        );
        List<News> listOfNews = tq.getResultList(); 
        
        em.close();
        emf.close();
        
        return listOfNews;        
    }
    
   /**
     * Pulls required News by it's ID
     * @param id Long of required news
     * @return News, object created from DB record
     */
    public News selectNews(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        News news = em.find(News.class, id);   
        
        em.close();
        emf.close();
        return news;
    }
    
     /**
     * Methods updates header of news
     * @param id_news Long id of required enrollment
     * @param header String
     */
    public void updateNewsHeader(Long id_news, String header) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        News news = em.find(News.class, id_news);
               
        et.begin();
        news.setNewsHeader(header);
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Methods updates text of news
     * @param id_news Long id of required enrollment
     * @param text String
     */
    public void updateNewsText(Long id_news, String text) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        News news = em.find(News.class, id_news);
               
        et.begin();
        news.setNewsText(text);
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Deletes current news by it's ID from DB
     * @param id Long
     */
    public void deleteNews(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        News news = em.find(News.class, id);   
               
        et.begin();
        em.remove(news);
        et.commit();
        
        em.close();
        emf.close();
    }
    
}
