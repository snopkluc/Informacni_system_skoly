/*
  Class is an entity for work with concrete year.
  School has always one class with original name and school year
  so according to this entity is possible to edit others like
  difficulty of subject or grade for an year.
*/
package model;


import java.util.List;
import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="school_class")
public class SchoolClass {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="number", nullable=false)
    private String number;
    
    //check with regEx - format 2014/2015
    @Column(name="year") 
    private String year;
    
    @OneToOne
    private SchoolUser classTeacher;
    
    @OneToMany(mappedBy="schoolClass")
    private List<SchoolUser> classStudents;

     /**
     * Basic constructor
     */
    public SchoolClass(){
        
    }
    
      /**
     * Extended constructor
     * @param number String
     * @param year String
     * @param classTeacher SchoolUser
     */
    public SchoolClass(String number, String year, SchoolUser classTeacher) {
        this.number = number;
        this.year = year;
        this.classTeacher = classTeacher;
    }
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public SchoolUser getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(SchoolUser classTeacher) {
        this.classTeacher = classTeacher;
    }

    public List<SchoolUser> getClassStudents() {
        return classStudents;
    }

    public void setClassStudents(List<SchoolUser> classStudents) {
        this.classStudents = classStudents;
    }  
}
