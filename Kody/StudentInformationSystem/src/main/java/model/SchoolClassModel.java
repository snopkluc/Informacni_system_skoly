/*
 Same posibilites as SchoolClass Class only allows us approach to
 informations saved in database
 */
package model;

import javax.persistence.*;

public class SchoolClassModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in SchoolClassModel table with single ID
     */
    public void createSchoolClass(String number, String year) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setNumber(number);
        schoolClass.setYear(year);        
        
        em.persist(schoolClass);
        et.commit();     
        
        em.close();
        emf.close();
    }
    /**
     * Pulls required SchoolClass by it's ID
     * @param id Long of required schoolClass
     * @return SchoolClass, object created from DB record
     */
    public SchoolClass selectSchoolClass(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        SchoolClass schoolClass = em.find(SchoolClass.class, id);   
        
        em.close();
        emf.close();
        return schoolClass;
    }
    
   /**
     * Methods updates school class number
     * @param id Long id of required enrollment
     * @param number String
     */
    public void updateSchoolClassNumber(Long id, String number) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolClass schoolClass = em.find(SchoolClass.class, id);
               
        et.begin();
        schoolClass.setNumber(number);
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Methods updates school class year
     * @param id Long id of required enrollment
     * @param year String
     */
     
    public void updateSchoolClassYear(Long id, String year) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolClass schoolClass = em.find(SchoolClass.class, id);
               
        et.begin();
        schoolClass.setYear(year);
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Methods updates school class teacher
     * @param id_schoolClass Long id of required enrollment
     * @param id_teacher Long
     */
     
    public void updateSchoolClassTeacher(Long id_schoolClass, Long id_teacher) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolClass schoolClass = em.find(SchoolClass.class, id_schoolClass);  
        SchoolUser teacher = em.find(SchoolUser.class, id_teacher);
               
        et.begin();
        schoolClass.setClassTeacher(teacher);
        teacher.setSchoolClassTeaching(schoolClass);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    /**
     * Method adds a school class student
     * @param id_schoolClass Long
     * @param id_student Long
     */
    public void addSchoolClassStudent(Long id_schoolClass, Long id_student) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        SchoolClass schoolClass = em.find(SchoolClass.class, id_schoolClass);
        SchoolUser student = em.find(SchoolUser.class, id_student);
        
        et.begin();
        schoolClass.getClassStudents().add(student);
        student.setSchoolClass(schoolClass);       
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Deletes current school class by it's ID from DB
     * @param id Long
     */
    public void deleteSchoolClass(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolClass schoolClass = em.find(SchoolClass.class, id);   
               
        et.begin();
        em.remove(schoolClass);
        et.commit();
        
        em.close();
        emf.close();
    }
}
    

