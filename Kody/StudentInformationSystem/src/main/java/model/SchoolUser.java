/*
 This class gets all informations about user for other actions in program.
*/
package model;


import java.util.Collection;
import java.util.List;
import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="school_user")
public class SchoolUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="username", nullable=false, unique = true)
    private String username;
    
    @Column(name="password", nullable=false)
    private String password;
    
    @Column(name="first_name", nullable=false)
    private String firstName;
    
    @Column(name="last_name", nullable=false)
    private String lastName;
    
    @Column
    private String address;

    @Column(name="user_role", nullable=false)
    private String userRole;
    
    @ManyToOne
    private SchoolClass schoolClass;
    
    @OneToMany(mappedBy="student")
    private List<EnrollmentStudent> enrollmentStudent;
    
    /*@ManyToMany(mappedBy="students")
    private Collection<Enrollment> enrollments;*/
    
    @OneToOne(mappedBy="classTeacher")
    private SchoolClass schoolClassTeaching;
    
    @OneToMany(mappedBy="teacher")
    private List<Enrollment> enrollmentsTeaching;
    
     /**
     * Basic constructor
     */
    public SchoolUser(){
        
    }
    
      /**
     * Extended constructor
     * @param firstName String
     * @param lastName String
     * @param address String
     * @param userRole String
     * @param schoolClass schoolClass
     */
    public SchoolUser(String firstName, String lastName, String address, String userRole, SchoolClass schoolClass) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.userRole = userRole;
        this.schoolClass = schoolClass;
    }
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String role) {
        this.userRole = role;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    /*public Collection<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(Collection<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }*/

    public List<EnrollmentStudent> getEnrollmentStudent() {
        return enrollmentStudent;
    }

    public void setEnrollmentStudent(List<EnrollmentStudent> enrollmentStudent) {
        this.enrollmentStudent = enrollmentStudent;
    }
    
    public SchoolClass getSchoolClassTeaching() {
        return schoolClassTeaching;
    }

    public void setSchoolClassTeaching(SchoolClass schoolClassTeaching) {
        this.schoolClassTeaching = schoolClassTeaching;
    }

    public List<Enrollment> getEnrollmentsTeaching() {
        return enrollmentsTeaching;
    }

    public void setEnrollmentsTeaching(List<Enrollment> enrollmentsTeaching) {
        this.enrollmentsTeaching = enrollmentsTeaching;
    }
}
