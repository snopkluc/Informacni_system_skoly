/*Example of a method for Model classes.
* Other methods in other classes are similar to this one. 
* Basic functionality is connection and queries to database to manipulate necessary data.
* This particular example is still non-functional.
* Received data part are sent to matching Controller in neccessary format.
*/
package model;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

public class SchoolUserModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;

    /**
     * Creates new record in SchoolUserModel table with single ID
     */
    public void createSchoolUser(String username, String password, String firstName, String lastName, String address, String userRole) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        SchoolUser schoolUser = new SchoolUser();
        schoolUser.setUsername(username);
        schoolUser.setPassword(password);
        schoolUser.setFirstName(firstName);
        schoolUser.setLastName(lastName);
        schoolUser.setAddress(address);
        schoolUser.setUserRole(userRole);
        
        em.persist(schoolUser);
        et.commit();     
        
        em.close();
        emf.close();
    }
    
    /**
     * Pulls required SchoolUser by it's ID
     * @param id Long of required schoolUser
     * @return SchoolUser, object created from DB record
     */
    public SchoolUser selectSchoolUser(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        SchoolUser schoolUser = em.find(SchoolUser.class, id);   
        
        em.close();
        emf.close();
        return schoolUser;
    }   
    
    public List<SchoolUser> selectAllStudents() {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        String role = "student";
        String query = "SELECT s FROM SchoolUser s"
                + "     WHERE s.userRole='"+role + "'";
        Query tq = em.createQuery(query);
        List<SchoolUser> listOfStudents = tq.getResultList();
        
        em.close();
        emf.close();
        return listOfStudents;
    }   
    
    public List<SchoolUser> selectBestStudents() {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        String query = "SELECT u,c from schoolUser u " +
            "join u.enrollmentStudent s " +
            "join s.enrollment e " +
            "join s.grade g" +
            "left join (select s1.student " +
            "           from enrollment_students s1 " +
            "           join s1.enrollment e1 " +
            "           left join s1.grade g1 " +
            "           where g1.mark is null " +
            "             or g1.mark > 2) x " +
            "on x.id = u.id " +
            "join u.schoolClass c " +
            "where x.id is null " +
            "group by u.lastName, u.firstName, u.id, c.year, c.number " +
            "having avg(g.mark) <= 1.5 " +
            "order by avg_mark, c.year desc, u.last_name, u.first_name";
        Query tq = em.createQuery(query);
        List<SchoolUser> listOfStudents = tq.getResultList();
        
        em.close();
        emf.close();
        return listOfStudents;
    }   
    
    
    public List<Enrollment> selectStudentListOfEnrollments(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolUser student = em.find(SchoolUser.class, id);  
        String query = "SELECT e FROM SchoolUser s"
                + "     JOIN s.enrollmentStudent es"
                + "     JOIN es.enrollment e"
                + "     WHERE s.id="+id;
        Query tq = em.createQuery(query);
        List<Enrollment> listOfEnrollments = tq.getResultList();
        
        em.close();
        emf.close();
        return listOfEnrollments;
    }
    
    public void updateSchoolUserFirstName(Long id, String firstName) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolUser schoolUser = em.find(SchoolUser.class, id);  
               
        et.begin();
        schoolUser.setFirstName(firstName);
        et.commit();
        
        em.close();
        emf.close();
    }
    
    public void updateStudentEnrollment(Long student_id, long enrollment_id) {
        
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        enrollmentModel.updateEnrollmentStudent(enrollment_id, student_id);
        
    }
    
    
    /**
     * Methods updates student school class
     * @param id_student Long id of required studentSchoolClass
     * @param id_schoolClass Long
     */
    public void updateStudentSchoolClass(Long id_student, Long id_schoolClass) {
        
        SchoolClassModel schoolClassModel = new SchoolClassModel();
        schoolClassModel.addSchoolClassStudent(id_schoolClass, id_student);
        
    }
   /**
     * Methods updates school user enrollment
     * @param id_schoolUser Long id of required enrollment
     * @param id_enrollment Long
     */
    /*public void updateSchoolUserEnrollment(Long id_schoolUser, Long id_enrollment) {
        
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        enrollmentModel.updateEnrollmentStudent(id_enrollment, id_schoolUser);
       
    }*/
   /**
     * Methods updates teacher school class who teach
     * @param id_teacher Long id of required enrollment
     * @param id_schoolClass Long
     */
    public void updateTeacherSchoolClassTeaching(Long id_teacher, Long id_schoolClass) {
        
        SchoolClassModel schoolClassModel = new SchoolClassModel();
        schoolClassModel.updateSchoolClassTeacher(id_schoolClass, id_teacher);
        
    }
    /**
     * Methods updates academic year in Enrollment
     * @param id_teacher Long id of required enrollment
     * @param id_enrollment Long
     */
    public void updateTeacherEnrollmentsTeaching(Long id_teacher, Long id_enrollment) {
        
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        enrollmentModel.updateEnrollmentTeacher(id_enrollment, id_teacher);
        
    }
    /**
     * Deletes current schoolUser by it's ID from DB
     * @param id Long
     */
    public void deleteSchoolUser(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        SchoolUser schoolUser = em.find(SchoolUser.class, id);  
        //find all enrollmentstudents
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete criteriaDelete = criteriaBuilder.createCriteriaDelete(EnrollmentStudent.class);
        Root enrollmentStudent = criteriaDelete.from(EnrollmentStudent.class);
        criteriaDelete.where(criteriaBuilder.equal(enrollmentStudent.get("student_id"), id));
        Query query = em.createQuery(criteriaDelete);
        int rowCount = query.executeUpdate();
               
        et.begin();
        em.remove(schoolUser);
        et.commit();
        
        em.close();
        emf.close();
    }
}
