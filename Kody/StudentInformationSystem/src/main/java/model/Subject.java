/*
 Subject is an entity which tells us name of subject. That's going to be an FK 
 for other entities, for example what subject does which teacher lecturing or
 specific lecture of week. 
*/
package model;

import java.util.List;
import javax.persistence.*;

/**
 * JPA annotation
 * Creates table "enrollment".
 */
@Entity
@Table(name="subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="subject_name", nullable=false, unique=true)
    private String subjectName;
    
    @OneToMany(mappedBy = "subject")
    private List<Enrollment> enrollments;
    
     /**
     * Basic constructor
     */
    public Subject(){
        
    }
    
      /**
     * Extended constructor
     * @param subjectName String
     */
    public Subject(String subjectName) {
        this.subjectName = subjectName;
    }
    /*Getters/setters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }
    
    

    
}
