/* 
   Same posibilites as Subject Class only allows us approach to
   informations saved in database
*/
package model;

import java.util.List;
import javax.persistence.*;

public class SubjectModel {
    
     /**Variables required in JPA to establish a connection with DB */
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Creates new record in SubjectModel table with single ID
     */
    public void createSubject(String subjectName) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        Subject subject = new Subject();
        subject.setSubjectName(subjectName);
        
        em.persist(subject);
        et.commit();     
        
        em.close();
        emf.close();
    }
    
    /**
     * Pulls required Subject by it's ID
     * @param id Long of required subject
     * @return Subject, object created from DB record
     */
    public Subject selectSubject(Long id) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   
        
        Subject subject = em.find(Subject.class, id);   
        
        em.close();
        emf.close();
        return subject;
    }
    
    public List<Subject> selectAllSubjects() {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();   

        String query = "SELECT s FROM Subject s";
        Query tq = em.createQuery(query);
        List<Subject> listOfSubjects = tq.getResultList();
        
        em.close();
        emf.close();
        return listOfSubjects;
        
    }
    
    /**
     * Methods updates subject name
     * @param id Long id of required enrollment
     * @param name String
     */
    public void updateSubjectName(Long id, String name) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Subject subject = em.find(Subject.class, id);
        et.begin();
        subject.setSubjectName(name);
        et.commit();
        
        em.close();
        emf.close();
    }
    
   /**
     * Method adds subject to enrollment
     * @param id_subject Long
     * @param id_enrollment Long
     */
    public void addSubjectEnrollment(Long id_subject, Long id_enrollment) {
        
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();

        Subject subject = em.find(Subject.class, id_subject);
        Enrollment enrollment = em.find(Enrollment.class, id_enrollment);
        
        et.begin();
        subject.getEnrollments().add(enrollment);
        enrollment.setSubject(subject);       
        et.commit();
        
        em.close();
        emf.close();
    }
    /**
     * Deletes current subject by it's ID from DB
     * @param id Long
     */
    public void deleteSubject(Long id) {
        emf = Persistence.createEntityManagerFactory("cz.cvut.fel_StudentInformationSystem_jarPersistence");        
        em = emf.createEntityManager();  
        EntityTransaction et = em.getTransaction();
        
        Subject subject = em.find(Subject.class, id);   
               
        et.begin();
        em.remove(subject);
        et.commit();
        
        em.close();
        emf.close();
    }
}
