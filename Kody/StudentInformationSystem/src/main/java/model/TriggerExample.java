/**
 * Example of DB trigger
 */
package model;

public class TriggerExample {
    
    public void trigger() {
        
        String query = "CREATE TRIGGER year_check AFTER INSERT ON school_class\n" +
                  "     BEGIN\n" +
                  "     IF NEW.year IS NULL THEN\n" +
                  "     SET NEW.year = '2016/2017';\n" +
                  "     END IF;\n" +
                  "     END;";
        
    }
    
}
