/**
 * Defines DB connection for JDBC, not used
 */
package utils;

import java.sql.*;

public class DBConnection {
      
    //change connection to other account?
    private static final String CONNECTION = "jdbc:postgresql://slon.felk.cvut.cz:5432/db17_praveale";
    private static final String USERNAME = "db17_praveale";
    private static final String PASSWORD = "db17_praveale";
    private Connection conn;
    public DBConnection() throws ClassNotFoundException, SQLException
    {    /** Creates a new instance of DatabaseConnection */
        Class.forName("org.postgresql.Driver");
        this.conn = DriverManager.getConnection(CONNECTION, USERNAME, PASSWORD);
    }
    public Connection getConnection()
    {
        return this.conn;
    }
    public void closeConnection() throws SQLException {
        conn.close();
    }
}
