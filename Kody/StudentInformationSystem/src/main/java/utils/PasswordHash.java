/*
Pretty common algorithm for hashing.
This class returns hexadecimal string representation for getHex hash of input argument
*/
package utils;

import java.io.UnsupportedEncodingException; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;  

public class PasswordHash { 
 
    private static String toHex(byte[] data) { 
        
        StringBuffer saveBuffer = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    saveBuffer.append((char) ('0' + halfbyte));
                else 
                    saveBuffer.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return saveBuffer.toString();
    } 
 
    public static String getHex(String text) 
    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
    MessageDigest md;
    md = MessageDigest.getInstance("SHA-1");
    byte[] hash = new byte[40];
    md.update(text.getBytes("iso-8859-1"), 0, text.length());
    hash = md.digest();
    return toHex(hash);
    } 
} 
