package utils;

/**
 * This server works on PURE MAGIC
*/
import controller.CommunicationController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;


public class Server {
   
    private ServerSocket serverSocket;
    private boolean running = true;
    
    /** Main method for Server part */
    public static void main(String[] args) {
        Server server = new Server();
    }

    /** Server constructor.
     * Opens port, establishes connection with new Clients and creates new thread
     * for every client
     */
    public Server() {
        try {
            this.serverSocket = new ServerSocket(25565);
            System.out.println("Server: online");

           
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (running) {
            try {
                Socket clientSocket = serverSocket.accept();
                ClientServerThread clientServerThread = new ClientServerThread(clientSocket);
                clientServerThread.start();
                
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }
        try {
            serverSocket.close();
            System.out.println("Server stopped");
        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    /** Thread for a client. */
    class ClientServerThread extends Thread {
        Socket currentClientSocket;
        boolean threadRunning = true;
        
        public ClientServerThread() {
            super();
        }
        
        ClientServerThread(Socket socket) {
            currentClientSocket = socket;
        }
        
        /** Method establishes streams for input and output communication with client.
         * Controls input and output messages.
         */
        public void run() {
            BufferedReader input = null;
            PrintWriter output = null;
            CommunicationController comm = new CommunicationController();
            String response;
            System.out.println("Accepted Client");

                        
            try {
                input = new BufferedReader(new InputStreamReader(currentClientSocket.getInputStream()));
                output = new PrintWriter(new OutputStreamWriter(currentClientSocket.getOutputStream()));


                while (threadRunning) {
             
                    String receivedMessage = input.readLine();
                    if (receivedMessage != null) {      
                        System.out.println("Server: received message from Client");
                        System.out.println("Received message is " + receivedMessage);

                        response = comm.analyzeReceivedMessage(receivedMessage);

                        
                        output.println(response);
                        output.flush();
                        
                    }               
                    
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    input.close();
                    output.close();
                    currentClientSocket.close();
                    System.out.println("Close thread");                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }                
            }           
        }       
    }
    /*******************************************************************/
    /*******************COMMUNICATION CONTROLLER************************/
    /*******************************************************************/
    /** Boolean variable representing the state of controller request. */
    private boolean requestSent = false;
    
    /** String variable representing request that will be sent to server */
    private String request;
    
    /** Server answer */
    private String result;
    
    private boolean resultError = false;
    
    private String response;

    public boolean isRequestSent() {
        return requestSent;
    }

    public void setRequestSent(boolean requestSent) {
        this.requestSent = requestSent;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }  
    
    public void sendRequest(JSONObject request) {
        
        setRequest(request.toString());
        setRequestSent(true);
        
    }

    public boolean isResultError() {
        return resultError;
    }

    public void setResultError(boolean resultError) {
        this.resultError = resultError;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
    
    
    public void generateEverythingRequest() {
        System.out.println("Client Comm: request received");
        JSONObject obj = new JSONObject();
        obj.put("OperationCode", "Generate");
        System.out.println("Client Comm: setting Request to Server");
        setRequest(obj.toString());
        setRequestSent(true);
        System.out.println("Client Comm: request to server is set");
        running = true;
        //clientExecute();
    }
    
}

