/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import model.SchoolUser;
import model.SchoolUserModel;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test SchoolUserModel
 */
public class SchoolUserModelJUnitTest {
    
    public SchoolUserModelJUnitTest() {
        
    }

    @Test 
    public void testFindExistingUser() {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        Long userId = Long.valueOf(1251);
        SchoolUser currentUser = schoolUserModel.selectSchoolUser(userId);
        assertNotNull(currentUser);
        
    }
    
    @Test
    public void testSelectSearchedUser() {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        Long userId = Long.valueOf(1251);
        SchoolUser currentUser = schoolUserModel.selectSchoolUser(userId);
        assertEquals(userId, currentUser.getId());
        
    }
    
    @Test
    public void testChangeFirstName() {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        Long userId = Long.valueOf(1251);
        schoolUserModel.updateSchoolUserFirstName(userId, "Jaroslava");
        
        SchoolUser currentUser = schoolUserModel.selectSchoolUser(userId);
        String newFirstName = currentUser.getFirstName();
        
        assertEquals("Jaroslava", newFirstName);
        
    }
    
    @Test
    public void testDeleteSchoolUser() {
        
        SchoolUserModel schoolUserModel = new SchoolUserModel();
        
        Long userId = Long.valueOf(1251);
        SchoolUser currentUser = schoolUserModel.selectSchoolUser(userId);
        assertNotNull(currentUser);
        
        schoolUserModel.deleteSchoolUser(userId);
        currentUser = schoolUserModel.selectSchoolUser(userId);
        assertNull(currentUser);       
        
    }
}
