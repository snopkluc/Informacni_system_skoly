/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.UserProfileController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test userProfileController 
 */
public class UserProfileControllerJUnitTest {
    
    public UserProfileControllerJUnitTest() {
    }
    

    @Test 
    public void testPrintFirstName() {
        
        UserProfileController userProfileController = new UserProfileController();
        Long userId = Long.valueOf(1451);
        assertEquals("Jan",  userProfileController.printFirstName(userId));       ;
        
    }
    @Test 
    public void testPrintLastName() {
        
        UserProfileController userProfileController = new UserProfileController();
        Long userId = Long.valueOf(1451);
        assertEquals("Sedlák",  userProfileController.printLastName(userId));       ;
        
    }
    @Test 
    public void testPrintAddress() {
        
        UserProfileController userProfileController = new UserProfileController();
        Long userId = Long.valueOf(1451);
        assertEquals("Vinohradská 5 Praha 2 14300",  userProfileController.printAddress(userId));       ;
        
    }
    @Test 
    public void testPrintRole() {
        
        UserProfileController userProfileController = new UserProfileController();
        Long userId = Long.valueOf(1451);
        assertEquals("teacher",  userProfileController.printRole(userId));       ;
        
    }
    @Test 
    public void testPrintUsername() {
        
        UserProfileController userProfileController = new UserProfileController();
        Long userId = Long.valueOf(1451);
        assertEquals("jansedlak",  userProfileController.printUsername(userId));       ;
        
    }
}
