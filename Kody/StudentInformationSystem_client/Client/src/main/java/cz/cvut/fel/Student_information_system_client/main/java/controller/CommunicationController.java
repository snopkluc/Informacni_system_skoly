/**
 * Provides communication between View and Server part. 
 * Creates a special query on basis of View actions, which is sent to server,
 * and if needed, receives the response of server, containing necessary info.
 */
package cz.cvut.fel.Student_information_system_client.main.java.controller;

import org.json.simple.JSONObject;

public class CommunicationController {
    
    /** Boolean variable representing the state of controller request. */
    private boolean requestSent = false;
    
    /** String variable representing request that will be sent to server */
    //private String request;
    private String request;
    
    /** Server answer */
    private String result;
    
    private boolean resultError = false;

    public boolean isRequestSent() {
        return requestSent;
    }

    public void setRequestSent(boolean requestSent) {
        this.requestSent = requestSent;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }  
    
    public void sendRequest(JSONObject request) {
        
        setRequest(request.toString());
        setRequestSent(true);
        
    }

    public boolean isResultError() {
        return resultError;
    }

    public void setResultError(boolean resultError) {
        this.resultError = resultError;
    }
    
    public void generateEverythingRequest() {
        System.out.println("Client Comm Controller: request received");
        JSONObject obj = new JSONObject();
        obj.put("OperationCode", "Generate");
        System.out.println("Client Comm Controller: setting Request to Server");
        setRequest(obj.toString());
        setRequestSent(true);
        System.out.println("Client Comm COntroller: request to server is set");
    }
    
}
