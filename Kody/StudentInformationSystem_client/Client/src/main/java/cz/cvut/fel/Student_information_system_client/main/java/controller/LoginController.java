/**
 * Class handles login process.
 * isLogged method
 * 
 */
package cz.cvut.fel.Student_information_system_client.main.java.controller;

import cz.cvut.fel.Student_information_system_client.main.java.utils.Client;
import cz.cvut.fel.Student_information_system_client.main.java.utils.PasswordHash;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LoginController {
    
    
    /**
     * Creates JSON object with username and password that is sent to server.
     * Analyzes the server response 
     * @param username String received from user input
     * @param password String received from user input
     * @return boolean based on successful/unsuccessful logging in the system
     */
    public boolean isLogged (String username, String password) {
        
        //password encrypt
        try {
            password = PasswordHash.getHex(password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        Client client = new Client();
        JSONObject obj = new JSONObject();
        obj.put("OperationCode","Login");
        obj.put("Username", username);
        obj.put("Password", password);
        String result = client.setNewClientRequest(obj.toString());
        
        
        //looks terrible :/
        /* Analyzes the response received by server.
        Returns false in cases:
        result is null, 
        server error is set, 
        object parsed from server response is null,
        or received message is "Error"
        */
        if ((result == null)) {
            return false;
        } else {            
            JSONParser parser = new JSONParser();   
            JSONObject resultObj = new JSONObject();
            String resultString = result;
            //comm.setResult(null);
            //comm.setResultError(false);
            
            //parse json from received result
            try {
                resultObj = (JSONObject) parser.parse(resultString);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
            //json object analysis
            if (resultObj != null) {
                String resultLogin = (String) resultObj.get("LoginResult");
                String resultError = (String) resultObj.get("Error");
                if (resultLogin.equals("Success")) {
                    System.out.println("Login success");
                    return true;
                } else if (resultLogin.equals("Not success")) {
                    System.out.println("Login not success");
                    return false;
                    
                } else if (resultError.equals("Error")){
                    System.out.println("Server returned error");
                    return false;
                } else {
                    System.out.println("some unknown response error");
                    return false;
                }
                
            } else {
                //if json object is null
                return false;
            }
                  
        }
    }  
}
