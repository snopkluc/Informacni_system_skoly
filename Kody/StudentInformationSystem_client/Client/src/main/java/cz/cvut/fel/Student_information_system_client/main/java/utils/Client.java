/**
 * This client works on PURE MAGIC
 */

package cz.cvut.fel.Student_information_system_client.main.java.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

public class Client {

    private Socket clientSocket;  
    private boolean running = false;

    public Client() {
        
    }
    
    private void clientExecute() {
        BufferedReader input = null;
        PrintWriter output = null;
        
        String requestToServer;
        
        try {
            this.clientSocket = new Socket("localhost", 25565);
            System.out.println("Client: online");
            output = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()));
            input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        while(running) {
            try {
                if (isRequestSent() && getRequest() != null) {
                    System.out.println("Client: request received");
                    requestToServer = getRequest();
                    output.println(requestToServer);
                    output.flush();
                    setRequest(null);
                    setRequestSent(false);
                    System.out.println("Client: request sent");
                    
                    String receivedMessage = input.readLine();  
                    
                    /* Improvised timeout */
                    long start = System.currentTimeMillis();
                    long end = start + 10*1000; // 10 seconds * 1000 ms/sec
                    
                    System.out.println("Client: trying to receive message from server");
                    while ((receivedMessage == null) && (System.currentTimeMillis() < end)) {
                        receivedMessage = input.readLine(); 
                    }          
                    System.out.println("Client: timeout or server answers");
                    if (receivedMessage != null) {
                        System.out.println("Client: server has responded");
                        setResult(receivedMessage);
                        System.out.println("Received message " + receivedMessage);
                        running = false;
                    } else {
                        System.out.println("Server does not answer");
                        setResult("No answer");
                        running = false;
                    }
                    
                } else if (!isRequestSent()) {
                    System.out.println("test lololo");
                }          
                
                
                
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                //running = false;
            }
        }
        try {
            input.close();
            output.close();
            clientSocket.close();
            System.out.println("Close thread");                    
        } catch (IOException ex) {
            ex.printStackTrace();
        }                
        
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
    
    
    
    /*******************************************************************/
    /*******************COMMUNICATION CONTROLLER************************/
    /*******************************************************************/
    /** Boolean variable representing the state of controller request. */
    private boolean requestSent = false;
    
    /** String variable representing request that will be sent to server */
    private String request;
    
    /** Server answer */
    private String result;
    
    private boolean resultError = false;

    public boolean isRequestSent() {
        return requestSent;
    }

    public void setRequestSent(boolean requestSent) {
        this.requestSent = requestSent;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }  
    
    public void sendRequest(JSONObject request) {
        
        setRequest(request.toString());
        setRequestSent(true);
        
    }

    public boolean isResultError() {
        return resultError;
    }

    public void setResultError(boolean resultError) {
        this.resultError = resultError;
    }
    
    /*public void generateEverythingRequest() {
        System.out.println("Client Comm: request received");
        JSONObject obj = new JSONObject();
        obj.put("OperationCode", "Generate");
        System.out.println("Client Comm: setting Request to Server");
        setRequest(obj.toString());
        setRequestSent(true);
        System.out.println("Client Comm: request to server is set");
        running = true;
        clientExecute();
    }*/
    
    public String setNewClientRequest(String messageRequest){
        setRequest(messageRequest);
        setRequestSent(true);
        running = true;
        clientExecute();
        return result;
    }
}
