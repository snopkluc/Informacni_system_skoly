/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.Student_information_system_client.main.java.view;

import cz.cvut.fel.Student_information_system_client.main.java.utils.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Admiral AokIji
 */
public class ClassListPanel extends javax.swing.JPanel {

    /**
     * Creates new form ClassList
     */
    public ClassListPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        schoolClassLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listOfBestStudentsTable = new javax.swing.JTable();
        refreshButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        listOfStudentsTable1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1024, 768));

        schoolClassLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        schoolClassLabel.setText("Seznam studentů");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setText("Seznam žáků");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        listOfBestStudentsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Jméno a příjmení", "ID"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(listOfBestStudentsTable);

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });

        listOfStudentsTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Jméno a příjmení", "ID"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(listOfStudentsTable1);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setText("Žáci s nejlepším vyhodnocením");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(schoolClassLabel)
                        .addGap(70, 70, 70)
                        .addComponent(refreshButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(95, 95, 95)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(431, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(804, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(schoolClassLabel)
                    .addComponent(refreshButton))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(400, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(140, 140, 140)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(398, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        Client client = new Client();
        JSONObject obj = new JSONObject();
        obj.put("OperationCode","Students");
        
        DefaultTableModel model = (DefaultTableModel) listOfStudentsTable1.getModel();
        
        String userInfo = client.setNewClientRequest(obj.toString());
        
        JSONParser parser = new JSONParser();   
        JSONObject resultObject = new JSONObject();
        try {
            resultObject = (JSONObject) parser.parse(userInfo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        JSONArray subjectsArray = (JSONArray) resultObject.get("Students");
        Iterator<JSONObject> iterator = subjectsArray.iterator();
        while (iterator.hasNext()) {
            JSONObject newRowObject = iterator.next();
            model.addRow(new Object[]{(String) (newRowObject.get("Name")).toString(), (String) (newRowObject.get("ID")).toString()});
        }
        
        displayBestStudents();
        
        
    }//GEN-LAST:event_refreshButtonActionPerformed

    public void displayBestStudents() {
        Client client = new Client();
        JSONObject obj = new JSONObject();
        obj.put("OperationCode","Best students");
        
        DefaultTableModel model = (DefaultTableModel) listOfBestStudentsTable.getModel();
        
        String userInfo = client.setNewClientRequest(obj.toString());
        
        JSONParser parser = new JSONParser();   
        JSONObject resultObject = new JSONObject();
        try {
            resultObject = (JSONObject) parser.parse(userInfo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        JSONArray subjectsArray = (JSONArray) resultObject.get("Students");
        Iterator<JSONObject> iterator = subjectsArray.iterator();
        while (iterator.hasNext()) {
            JSONObject newRowObject = iterator.next();
            model.addRow(new Object[]{(String) (newRowObject.get("Name")).toString(), (String) (newRowObject.get("ID")).toString()});
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable listOfBestStudentsTable;
    private javax.swing.JTable listOfStudentsTable1;
    private javax.swing.JButton refreshButton;
    private javax.swing.JLabel schoolClassLabel;
    // End of variables declaration//GEN-END:variables

}