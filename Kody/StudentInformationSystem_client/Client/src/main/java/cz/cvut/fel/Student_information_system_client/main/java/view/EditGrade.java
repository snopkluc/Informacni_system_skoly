/*
* class shoul've be opened when edit button was clicked
* (in ListOfSubject class)
* makes possible to edit or delete Grades at students 
* profiles
 */
package cz.cvut.fel.Student_information_system_client.main.java.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class EditGrade extends JFrame {

    JComboBox gradeBox, subjectBox, yearBox, studentBox;
    JLabel gradeLabel, subjectLabel, yearLabel, studentLabel;
    JButton deleteButton, editButton;
    JPanel panel;
    
    public EditGrade()
    {
        super("Editace známky"); //parametr je titulek okna
        
        gradeLabel = new JLabel();
        gradeLabel.setText("Znamka");
        gradeBox = new JComboBox<>();
        gradeBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5"  }));
        
        subjectLabel = new JLabel();
        subjectLabel.setText("Predmet");
        subjectBox = new JComboBox<>();
        
        yearLabel = new JLabel();
        yearLabel.setText("Rocnik");
        yearBox = new JComboBox<>();
        
        studentLabel = new JLabel();
        studentLabel.setText("Zak");
        studentBox = new JComboBox<>();
        studentBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { ""  }));
        
         editButton = new JButton("Edit");
         deleteButton = new JButton("Smazat");
         
        panel = new JPanel(new GridLayout(5,1));
        panel.add(gradeLabel);
        panel.add(gradeBox);
        panel.add(subjectLabel);
        panel.add(subjectBox);
        panel.add(yearLabel);
        panel.add(yearBox);
        panel.add(studentLabel);
        panel.add(studentBox);
        panel.add(deleteButton);
        panel.add(editButton);
        add(panel,BorderLayout.CENTER);
        editButton.addActionListener((ActionListener) this);
        deleteButton.addActionListener((ActionListener) this);
        
    }
        
     /*public void actionPerformed(ActionEvent ae) {
             
         }*/

    /*public static void main(String[] args) {
        EditGrade okno = new EditGrade();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
        okno.setSize(300, 200);
        okno.setLocationRelativeTo(null);
    }*/
}



