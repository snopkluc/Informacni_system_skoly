/*
* makes possible to edit News on main page
*/
package cz.cvut.fel.Student_information_system_client.main.java.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

    public class EditNews extends JFrame {
        JTextField nameField, newsNameField, containField;
        JLabel nameLabel, newsNameLabel, containLabel, newsIdLabel, noteLabel;
        JButton saveButton, deleteButton;
        JPanel panel, panel2;
        JTextPane containPane;
        JTextArea textArea;
        JComboBox newsIdBox;

        public EditNews(){
            super("Editace novinky");
        
            newsIdLabel = new JLabel();
            newsIdLabel.setText("ID novinky");
            newsIdBox = new JComboBox<>();
            newsIdBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5"  }));
            nameLabel = new JLabel();
            nameLabel.setText("Jmeno");
            nameField = new JTextField(15);
            
            newsNameLabel = new JLabel();
            newsNameLabel.setText("Nazev novinky");
            newsNameField = new JTextField(15);
           
            
            containLabel = new JLabel();
            containLabel.setText("Text");
           
            
            textArea = new JTextArea(20, 60);
            JScrollPane scrollPane = new JScrollPane(textArea); 
            textArea.setEditable(true);
            
            saveButton = new JButton();
            saveButton.setText("Ulozit");
            deleteButton = new JButton();
            noteLabel = new JLabel();
            noteLabel.setText("Pro smazani novinky vyberte ID a klinete na smazat.");
            deleteButton.setText("Smazat");
            JLabel not = new JLabel();
            
            
            panel = new JPanel(new GridLayout(6,2));
            panel.add(nameLabel);
            panel.add(nameField);
            panel.add(newsNameLabel);
            panel.add(newsNameField);
            panel.add(containLabel);
            panel.add(scrollPane);
            panel.add(saveButton);
            panel.add(not);
            panel.add(newsIdLabel);
            panel.add(newsIdBox);
            panel.add(noteLabel);
            panel.add(deleteButton);
            add(panel, BorderLayout.CENTER);
            saveButton.addActionListener((ActionListener) this);
            deleteButton.addActionListener((ActionListener) this);
        }
 
        
         public void actionPerformed(ActionEvent ae) {
             
         }
      
        public void displayEditNews() {
            EditNews okno = new EditNews();
            okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            okno.setVisible(true);
            okno.setSize(600, 300);
            okno.setLocationRelativeTo(null);
        }
    }