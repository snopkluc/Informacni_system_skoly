/**
 * Login screen, part of the interface; written in code, not in the NetBeans designer
 */
package cz.cvut.fel.Student_information_system_client.main.java.view;


import cz.cvut.fel.Student_information_system_client.main.java.controller.LoginController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


class Login extends JFrame implements ActionListener{

    JButton SUBMIT;
    JPanel panel;
    JLabel usernameLabel,passwordLabel;
    final JTextField usernameText,passwordText;

    /**
     * Method that creates all visual elements on the screen
     */
    Login() {
       usernameLabel = new JLabel();
       usernameLabel.setText("Username:");
       usernameText = new JTextField(15);

       passwordLabel = new JLabel();
       passwordLabel.setText("Password:");
       passwordText = new JPasswordField(15);

       SUBMIT=new JButton("SUBMIT");

       panel=new JPanel(new GridLayout(3,1));
       panel.add(usernameLabel);
       panel.add(usernameText);
       panel.add(passwordLabel);
       panel.add(passwordText);
       panel.add(SUBMIT);
       add(panel,BorderLayout.CENTER);

       SUBMIT.addActionListener(this);
       setTitle("LOGIN FORM");
    }

    /**
     * Handles the process of login. 
     * Sends user input to server and controls the result
     * If OK, lets user in the main program.
     * @param ae ActionEvent
     */
    public void actionPerformed(ActionEvent ae){
        String username=usernameText.getText();
        String password=passwordText.getText(); 
        
        LoginController lgController = new LoginController();
        boolean loginSuccess = lgController.isLogged(username, password);
        
        if (loginSuccess) {
                  MainFrame frame = new MainFrame();
                  frame.setVisible(true);
                  this.setVisible(false);

        } else {
             System.out.println("enter the valid username and password");
            JOptionPane.showMessageDialog(this,"Incorrect login or password or Server does not respond",
            "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
}

/** Contains Main method */
public class LoginDemo {
    public static void main(String arg[]){
        
        try {            
            Login page=new Login();
            page.setSize(300,100);
            page.setVisible(true);
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}
