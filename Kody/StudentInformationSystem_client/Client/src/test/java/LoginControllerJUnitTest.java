

import cz.cvut.fel.Student_information_system_client.main.java.controller.LoginController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class LoginControllerJUnitTest {
    
    public LoginControllerJUnitTest() {
    }
    
    @Test
    public void testSuccessfulLogin() {        
        LoginController loginController = new LoginController();
        assertEquals(true, loginController.isLogged("jansedlak", "jansedlak"));
                
    }
    @Test
    public void testUnsuccessfulLogin() {        
        LoginController loginController = new LoginController();
        assertEquals(false, loginController.isLogged("jansedlak", "somestrangetext"));
                
    }
}
