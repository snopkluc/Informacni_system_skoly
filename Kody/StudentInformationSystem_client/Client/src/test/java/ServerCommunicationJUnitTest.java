/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cz.cvut.fel.Student_information_system_client.main.java.utils.Client;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admiral AokIji
 */
public class ServerCommunicationJUnitTest {
    
    public ServerCommunicationJUnitTest() {
    }
    
    @Test
    public void testSendErrorOperationCodeMessageToServer() {
        
        Client client = new Client();
        JSONObject obj = new JSONObject();
        obj.put("OperationCode", "somemeaninglesstext");
        String result = client.setNewClientRequest(obj.toString());
        
        JSONParser parser = new JSONParser();
        JSONObject resultObj = new JSONObject();
        try {
            resultObj = (JSONObject) parser.parse(result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        String serverMessage = (String) resultObj.get("Error");
        
        assertEquals("Error", serverMessage);
        
        
    }
    
    
}
