## Základní funkce:  
* Přihlášení, odhlášení
* User profile - display info
* Class list - select students and teacher from DB
* Enrollments list(for class) - select enrollments(by year) with teachers and grade(if exists)
* News - show on homepage, select all for current class
* Schedule - select lessons for current class (choose format)
* Add grade, news(change)
* Add enrollments for class
* Beginning of new academic year -> change class' year, clean list of enrollments


